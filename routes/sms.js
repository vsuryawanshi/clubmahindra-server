var express = require('express');
var router = express.Router();
var kickbox = require('kickbox').client('live_d98f0635e71ff1051ae2aec504cce2c3b82fc06cf6fcad37c1af74b09c367d1f').kickbox();
var twilio = require('twilio');
var db = require("../db");
var crypto = require("crypto");
var ObjectID = require('mongodb').ObjectID;

var accountSid = 'ACe354a9563e07074d8a6aa43ce07e9782'; 
var authToken = 'f6e97adf7c34bb0aa85b0cd5f9f07aad';  

var client = new twilio(accountSid, authToken);

router.post('/checkemail', function (req, res, next) {
    var emailToCheck = req.body.email;
    if(emailToCheck !== ""){
        kickbox.verify(emailToCheck, function (err, response) {
            // Let's see some results
            console.log(response.body);
            if(err){
                res.status(500).send(err);
            } else {
                res.send(response.body);
            }
        });
    }
});

router.post("/sendsms",function (req, res, next) {
    var phoneNumber = req.body.pn;
    var id = crypto.randomBytes(5).toString('hex');
    if(phoneNumber !== ""){
        client.messages.create({
            body: 'Please verify here : http://62.210.93.54:1212/' + id,
            to: phoneNumber,
            from: '+17744620164'
        })
        .then((message) => {
            console.log(message);
            var idgiven = message.body.substring(message.body.lastIndexOf("/")+1, message.body.length);
            //since sms has been sent, make a entry in the db
            var collection = db.get().db().collection("smstrack");
            collection.insertOne({phoneNumber:phoneNumber,status:"Pending",messageId:idgiven}, function(err,response){
                if(err){
                    res.status(500).send(err);
                } else {
                    res.send("success");
                }
            })
        }).catch(err => {
            console.log(err);
            res.status(500).send("Error");
        });
    }
});

router.post("/verify",function (req, res, next){
    var uid = req.body.id;
    var collection = db.get().db().collection("smstrack");
    collection.updateOne({messageId:uid},{$set:{status:"verified", info:req.body.info}}, function(err,response){
        if(err){
            res.status(500).send(err);
        } else {
            res.send(response);
        }
    })
})

module.exports = router;