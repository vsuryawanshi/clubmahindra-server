var express = require('express');
var router = express.Router();
var bcrypt = require('bcrypt');
var ObjectID = require('mongodb').ObjectID;
var db = require("../db");
var BCRYPT_SALT_ROUNDS = 12;


/* GET users listing. */
router.get('/', function (req, res, next) {
	var collection = db.get().db().collection("user");
	collection.find({}).project({"username":1.0}).toArray(function(err,users){
		if(err){
			res.status(500).send(err);
		} else {
			res.send(users);
		}
	})
});

router.post("/add",function(req,res,next){
	var uname = req.body.username;
	var pass = req.body.password;
	bcrypt.hash(pass, BCRYPT_SALT_ROUNDS).then(function(hashedPassword) {
		var collection = db.get().db().collection("user");
		collection.insertOne({username:uname,password:hashedPassword}, function(err,response){
			if(err){
				res.status(500).send(err);
			} else {
				res.send("success");
			}
		})
    });
})


//get all user collection mapping
router.get("/usercmap",function(req,res,next){
	var collection = db.get().db().collection("user_collection_map");
	collection.find({}).toArray(function(err,maps){
		if(err){
			res.status(500).send(err);
		} else {
			res.send(maps);
		}
	})
})


router.post("/updatecmap", function(req,res,next){
	var collection = db.get().db().collection("user_collection_map");
	collection.updateOne({_id:ObjectID(req.body.mapId)},{$set:{collection_name:req.body.newCollectionName}},{upsert:true}, function(err,response){
		if(err){
            res.status(500).send(err);
        } else {
            res.send(response);
        }
	})
});

router.post("/delete", function(req,res,next){
	var collection = db.get().db().collection("user");
	collection.deleteOne({_id:ObjectID(req.body.id)}, function(err, response){
		if(err){
            res.status(500).send(err);
        } else {
            res.send(response);
        }
	})
})


router.post("/addcmap", function(req,res,next){
	var collection = db.get().db().collection("user_collection_map");
	collection.updateOne({user_id:req.body.userId},{$set:{user_id:req.body.userId,collection_name:req.body.collectionName,username:req.body.userName}},{upsert:true}, function(err,response){
		if(err){
			res.status(500).send(err);
		} else {
			res.send(response);
		}
	})
})

module.exports = router;
